#  Magic Card Generator
Created by Matthew Wheeler

![](https://bitbucket.org/MatthewWheels/magiccard_generator/raw/d690fc7aeaa8983737e3bc2caaf12dfff50fa1b1/bin/data/ScreenShot.png)

This programs generates a Magic the Gathering playing card artwork based upon information from the webcam.

Base Code by Porfessor Coleman
[Link to Base Code](https://bitbucket.org/professorColeman/runwayml_stylegan_example/src/master/)

