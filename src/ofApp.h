#pragma once

#include "ofMain.h"
#include "ofxRunway.h"


// make sure you make your ofApp or class to inherit from ofxRunwayListener
class ofApp : public ofBaseApp, public ofxRunwayListener{
    
    
public:
    void setup();
    void update();
    void draw();
    
    void keyReleased(int key);
    
    ofxRunway runway;
    ofVideoGrabber webCam;
    ofImage runwayResult, framez;
   
    vector<float> v;
    //float v[512];
    
    
    // Callback functions that process what Runway sends back
    void runwayInfoEvent(ofJson& info);
    void runwayErrorEvent(string& message);
    
};
